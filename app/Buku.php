<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = ["judul", "deskripsi", "tahun", "penulis_id", "kategori_id"];

    public function kategori(){
        return $this->belongsTo('App\Kategori');
    }

    public function penulis(){
        return $this->belongsTo('App\Penulis');
    }

    public function peminjaman(){
        return $this->hasMany('App\Peminjaman');
    }
}
