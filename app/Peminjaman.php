<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = "peminjaman";
    protected $fillable = ["id_buku", "id_anggota", "id_user", "tanggal_pinjam", "tanggal_kembali"];

    public function buku(){
        return $this->belongsTo('App\Buku','id_buku');
    }

    public function anggota(){
        return $this->belongsTo('App\Anggota','id_anggota');
    }

    public function user(){
        return $this->belongsTo('App\User','id_user');
    }
}
