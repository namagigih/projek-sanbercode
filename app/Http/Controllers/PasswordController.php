<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class PasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(){
        return view('password.edit');
    }

    public function update(PasswordRequest $request){
        $request->user()->update([
            'password' => Hash::make($request->get('password'))
        ]);
        return redirect()->route('ubah-password')->with('success','Kata sandi berhasil diubah!');
    }
}
