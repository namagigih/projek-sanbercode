<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Anggota;
use App\Penulis;
use App\Peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $anggota = Anggota::count();
        $buku = Buku::count();
        $penulis = Penulis::count();
        $peminjam = Peminjaman::count();
        return view('dashboard.dashboard',compact('anggota','buku','penulis','peminjam'));
    }
}
