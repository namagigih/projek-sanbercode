<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(){
        $user = User::where('id',Auth::user()->id)->get();
        return view('profile.edit',compact('user'));
    }

    public function update(Request $request, $redirect){
        $data = $request->all();

        $item = auth()->user();

        $item->update($data);

        return redirect()->route($redirect)->with('success','Data berhasil diubah!');
    }
}
