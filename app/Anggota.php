<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{

    protected $table = 'anggota';

    protected $fillable = [
        'nama', 'no_hp', 'alamat', 'jenis_kelamin'
    ];

    public function peminjaman(){
        return $this->hasMany('App\Peminjaman');
    }
}
