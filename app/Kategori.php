<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';

    protected $fillable = [
        'nama'
    ];

    public function peminjaman(){
        return $this->hasMany('App\Peminjaman');
    }
}
