<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'DashboardController@index')->name('home');

Route::get('/home', 'DashboardController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/anggota', AnggotaController::class);
    Route::resource('/kategori', KategoriController::class);
    Route::resource('/penulis', PenulisController::class);
    Route::resource('/buku', BukuController::class);
    Route::resource('/peminjaman', PeminjamanController::class);

    Route::get('/dashboard','DashboardController@index')->name('dashboard');

    Route::get('/profile','ProfileController@edit')->name('profile');
    Route::post('/profile/{redirect}', 'ProfileController@update')->name('change-profile');

    Route::get('ubah-password', 'PasswordController@edit')->name('ubah-password');
    Route::post('ubah-password', 'PasswordController@update')->name('update-password');
    Route::get('/logout', 'HomeController@logout');
});

