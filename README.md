**ERD**<br>

![alt text](https://gitlab.com/namagigih/projek-sanbercode/-/raw/main/erd/erd.drawio.png?raw=true)


**1. Informasi Kelompok**<br>
Nomor Kelompok : 5<br>
Anggota : Yusup Hidayat dan Gigih Ridho Ridhana<br>
Tema : Tema Pilihan 1: CRUD Peminjaman Buku<br>

**2. Link Deploy**<br>
Heroku : http://sipb.herokuapp.com/

**3. Link Video Demo**<br>
Youtube : https://youtu.be/fAS3UUKJv60
