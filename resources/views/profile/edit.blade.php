@extends('layouts.master')

@push('style')
  <link rel="stylesheet" href="{{asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css') }}">
@endpush

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Anggota</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('anggota.index') }}">Anggota</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">

        <div class="row">
            <div class="col">
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Update Profil</h3>
                </div>

                <!-- Form -->
                <div class="container">
                    <form action="{{ route('change-profile','profile') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama" class="form-control-label">Nama</label>
                            <input class="form-control" type="text" id="name" name="name" value="{{ Auth::user()->name }}">

                            @error('name')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="email" class="form-control-label">Email</label>
                            <input class="form-control" type="email" id="email" name="email" value="{{ Auth::user()->email }}">

                            @error('email')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror

                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update Profil') }}
                            </button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
</div>
@endsection
@push('script')
<script src="{{asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js')}}"></script>
<script>
    @if (session('success'))
    swal.fire({
        title: 'Berhasil!',
        text: '{{ session('success') }}',
        buttonsStyling: false,
      confirmButtonClass: 'btn btn-success',
      icon: 'success'
    });
    @endif
</script>
@endpush
