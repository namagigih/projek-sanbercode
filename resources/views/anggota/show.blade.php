@extends('layouts.master')

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Anggota</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('anggota.index') }}">Anggota</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lihat Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <a href="{{ route('anggota.index') }}" class="btn btn-success mb-2 "> Kembali</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Data Anggota</h3>
                </div>

                <!-- Form -->
                <div class="container">
                    @foreach ($anggota as $key => $value)
                    <form action="#">
                        <div class="form-group">
                            <label for="nama" class="form-control-label">Nama</label>
                            <input class="form-control" type="text" id="nama" name="nama" value="{{ $value->nama }}" disabled>

                        </div>
                        <div class="form-group">
                            <label for="no_hp" class="form-control-label">No HP</label>
                            <input class="form-control" type="number" id="no_hp" name="no_hp" value="{{ $value->no_hp }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="alamat" class="form-control-label">Alamat</label>
                            <input class="form-control" type="text" id="alamat" name="alamat" value="{{ $value->alamat }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="status" class="form-control" disabled>
                                <option value="1"
                                        @if (old('jenis_kelamin', $value->jenis_kelamin) == "1") selected="selected" @endif>Laki-Laki
                                </option>
                                <option value="0"
                                        @if (old('jenis_kelamin',$value->jenis_kelamin) == "0") selected="selected" @endif>
                                    Perempuan
                                </option>
                            </select>
                        </div>
                    </form>
                    @endforeach
                </div>
              </div>
            </div>
          </div>
</div>
@endsection
