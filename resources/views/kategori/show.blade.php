@extends('layouts.master')

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Kategori</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('kategori.index') }}">Kategori</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lihat Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <a href="{{ route('kategori.index') }}" class="btn btn-success mb-2 "> Kembali</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Data Kategori</h3>
                </div>

                <!-- Form -->
                <div class="container">
                    @foreach ($kategori as $key => $value)
                    <form action="#">
                        <div class="form-group">
                            <label for="nama" class="form-control-label">Kategori</label>
                            <input class="form-control" type="text" id="nama" name="nama" value="{{ $value->nama }}" disabled>

                        </div>

                    </form>
                    @endforeach
                </div>
              </div>
            </div>
          </div>
</div>
@endsection
