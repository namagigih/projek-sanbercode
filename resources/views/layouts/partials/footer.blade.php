    <!-- Page content -->


        <!-- Footer -->
        <footer class="footer pt-0 fixed-bottom">
          <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-12">
              <div class="copyright text-center  ">
                &copy; 2021 <a href="https://gitlab.com/namagigih/projek-sanbercode" class="font-weight-bold ml-1" target="_blank">Kelompok 5</a>
              </div>
            </div>
          </div>
        </footer>
