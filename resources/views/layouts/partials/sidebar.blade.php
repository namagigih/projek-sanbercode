<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="{{ asset('assets/img/brand/sipb2-removebg-preview.png') }}" class="navbar-brand-img" alt="..." style="width:80px;">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link " href="{{ url('dashboard') }}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('anggota') }}">
                <i class="ni ni-single-02 text-orange"></i>
                <span class="nav-link-text">Kelola Anggota</span>
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('kategori') }}">
                  <i class="ni ni-tag text-primary"></i>
                  <span class="nav-link-text">Kelola Kategori Buku</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('penulis') }}">
                  <i class="ni ni-badge text-primary"></i>
                  <span class="nav-link-text">Kelola Penulis Buku</span>
                </a>
              </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('buku') }}">
                <i class="ni ni-books text-primary"></i>
                <span class="nav-link-text">Kelola Buku</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('peminjaman') }}">
                <i class="ni ni-archive-2 text-yellow"></i>
                <span class="nav-link-text">Kelola Peminjaman Buku</span>
              </a>
            </li>

        </div>
      </div>
    </div>
  </nav>
