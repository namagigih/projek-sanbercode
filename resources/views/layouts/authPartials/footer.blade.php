<footer class="py-5" id="footer-main">
  <div class="container">
    <div class="row align-items-center justify-content-xl-between">
      <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
          &copy; 2021 <a href="https://gitlab.com/namagigih/projek-sanbercode" class="font-weight-bold ml-1" target="_blank">Kelompok 5</a>
        </div>
      </div>

    </div>
  </div>
</footer>