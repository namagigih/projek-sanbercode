@extends('layouts.master')

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Buku</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('buku.index') }}">Buku</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lihat Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <a href="{{ route('buku.index') }}" class="btn btn-success mb-2 "> Kembali</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Data Buku</h3>
                </div>

                <!-- Form -->
                <div class="container">
                    @foreach ($buku as $key => $value)
                    <form action="#">
                        <div class="form-group">
                            <label for="judul" class="form-control-label">Judul</label>
                            <input class="form-control" type="text" id="judul" name="judul" value="{{ $value->judul }}" disabled>

                        </div>
                        <div class="form-group">
                            <label for="no_hp" class="form-control-label">Deskripsi</label>
                            <textarea class="form-control" disabled>{!! $value->deskripsi !!}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="alamat" class="form-control-label">Tahun</label>
                            <input class="form-control" type="text" id="alamat" name="alamat" value="{{ $value->tahun }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin">Penulis</label>
                            <input class="form-control" type="text" id="alamat" name="alamat" value="{{ $value->penulis->nama }}" disabled>
                        </div>
                        <div class="form-group">
                          <label for="jenis_kelamin">Kategori</label>
                          <input class="form-control" type="text" id="alamat" name="alamat" value="{{ $value->kategori->nama }}" disabled>
                      </div>
                    </form>
                    @endforeach
                </div>
              </div>
            </div>
          </div>
</div>
@endsection
