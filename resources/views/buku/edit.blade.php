@extends('layouts.master')
@push('style')
  <link rel="stylesheet" href="{{asset('assets/vendor/quill/dist/quill.core.css')}}" type="text/css">
@endpush
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Buku</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('buku.index') }}">Buku</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">

        <div class="row">
            <div class="col">
                <a href="{{ route('buku.index') }}" class="btn btn-success mb-2 "> Kembali</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Update Buku</h3>
                </div>

                <!-- Form -->
                <div class="container">
                    <form action="{{ route('buku.update',$buku->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label for="judul" class="form-control-label">Judul</label>
                          <input class="form-control" type="text" id="judul" name="judul" value="{{ $buku->judul }}" placeholder="Masukkan Judul Buku">

                          @error('judul')
                          <div class="alert alert-danger">
                              {{ $message }}
                          </div>
                          @enderror

                      </div>
                      <div class="form-group">
                        <label for="deskripsi" class="form-control-label">Dekripsi</label>
                        <textarea id="mytextarea" name="deskripsi">{!! $buku->deskripsi !!}</textarea>

                        @error('deskripsi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label for="tahun" class="form-control-label">Tahun</label>
                        <input class="form-control" type="number" id="tahun" name="tahun" value="{{ $buku->tahun }}" placeholder="Masukkan Tahun">

                        @error('tahun')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label for="penulis">Penulis</label>
                        <select name="penulis" id="penulis" class="form-control">
                          <option value="">--Pilih Penulis--</option>
                          @foreach ($penulis as $item => $value)
                            <option value="{{ $value->id }}" 
                              @if ($buku->penulis_id == $value->id) selected="selected" @endif>{{ $value->nama }}</option> 
                          @endforeach
                        </select>

                        @error('penulis')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror

                    </div>

                    <div class="form-group">
                      <label for="kategori">Kategori</label>
                      <select name="kategori" id="kategori" class="form-control">
                        <option value="">--Pilih Kategori--</option>
                        @foreach ($kategori as $item => $value)
                          <option value="{{ $value->id }}"
                            @if ($buku->kategori_id == $value->id) selected="selected" @endif>{{ $value->nama }}</option> 
                        @endforeach
                      </select>

                      @error('kategori')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                      @enderror

                  </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update Data') }}
                            </button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
</div>
@endsection
@push('script') 
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

  <script type="text/javascript">
    tinymce.init({
      selector: '#mytextarea'
    });
    </script>
@endpush
