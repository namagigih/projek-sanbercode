@extends('layouts.master')
@push('style')
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css')}}">
@endpush

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Buku</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Buku</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">

        <div class="row">
          {{-- <div class="col-xl-12 m-4"> --}}
            <div class="col">
                <a href="{{ route('buku.create') }}" class="btn btn-warning mb-2 ">+ Tambah Buku</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Data Buku</h3>
                </div>
                <!-- Light table -->
                <div class="table-responsive">
                  <table id="example1" class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" class="sort" data-sort="no">No</th>
                        <th scope="col" class="sort" data-sort="name">Judul</th>
                        <th scope="col" class="sort" data-sort="no_hp">Deskripsi</th>
                        <th scope="col" class="sort" data-sort="alamat">Tahun</th>
                        <th scope="col" class="sort" data-sort="jenis_kelamin">Penulis</th>
                        <th scope="col" class="sort" data-sort="jenis_kelamin">Kategori</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($buku as $key => $value)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ substr($value->judul,0,20) }} ...</td>
                            <td>{!! substr($value->deskripsi,0,20) !!} ...</td>
                            <td>{{ $value->tahun }}</td>
                            <td>{{ $value->penulis->nama }}</td>
                            <td>{{ $value->kategori->nama }}</td>
                            <td>
                                <form action="{{ route('buku.destroy',$value->id) }}" method="POST">
                                    <a href="{{ route('buku.show',$value->id) }}" class="btn btn-primary btn-sm">Show</a>
                                    <a href="{{ route('buku.edit',$value->id) }}" class="btn btn-success btn-sm">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr scope="col">
                            <td>No data</td>
                        </tr>
                        @endforelse

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
      </div>
        </div>
</div>
@endsection
@push('script')
  <script src="{{asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
  <script src="{{asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js')}}"></script>
  <script>
    $(function () {
        $("#example1").DataTable();
    });

    @if (session('success'))
    swal.fire({
      title: 'Berhasil!',
      text: '{{ session('success') }}',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-success',
      icon: 'success'
    });
    @endif

  </script>
@endpush
