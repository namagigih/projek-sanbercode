@extends('layouts.master')

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Peminjaman</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('peminjaman.index') }}">Peminjaman</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lihat Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <a href="{{ route('peminjaman.index') }}" class="btn btn-success mb-2 "> Kembali</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Data Peminjam</h3>
                </div>

                <!-- Form -->
                <div class="container">
                    @foreach ($peminjaman as $key => $value)
                    <form action="#">
                        <div class="form-group">
                            <label for="buku" class="form-control-label">Judul Buku</label>
                            <input class="form-control" type="text" id="buku" name="buku" value="{{ $value->buku->judul }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="anggota" class="form-control-label">Nama Anggota</label>
                            <input class="form-control" type="text" id="anggota" name="anggota" value="{{ $value->anggota->nama }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="user">Nama Petugas</label>
                            <input class="form-control" type="text" id="user" name="user" value="{{ $value->user->name }}" disabled>
                        </div>
                        <div class="form-group">
                          <label for="tanggal_pinjam">Tanggal Pinjam</label>
                          <input class="form-control" type="text" id="tanggal_pinjam" name="tanggal_pinjam" value="{{ $value->tanggal_pinjam }}" disabled>
                      </div>
                      <div class="form-group">
                        <label for="tanggal_kembali">Tanggal Kembali</label>
                        <input class="form-control" type="text" id="tanggal_kembali" name="tanggal_kembali" value="{{ $value->tanggal_kembali }}" disabled>
                    </div>
                    </form>
                    @endforeach
                </div>
              </div>
            </div>
          </div>
</div>
@endsection
