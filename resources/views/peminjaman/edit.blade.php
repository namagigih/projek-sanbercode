@extends('layouts.master')
@push('style')
  <link rel="stylesheet" href="{{asset('assets/vendor/quill/dist/quill.core.css')}}" type="text/css">
@endpush
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Peminjaman</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('peminjaman.index') }}">Peminjaman</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">

        <div class="row">
            <div class="col">
                <a href="{{ route('peminjaman.index') }}" class="btn btn-success mb-2 "> Kembali</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Update Peminjaman</h3>
                </div>

                <!-- Form -->
                <div class="container">
                    <form action="{{ route('peminjaman.update',$peminjaman->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label for="buku" class="form-control-label">Judul Buku</label>
                          <select name="buku" id="buku" class="form-control">
                            <option value="">--Pilih Judul Buku--</option>
                            @foreach ($buku as $item => $value)
                              <option value="{{ $value->id }}"
                                @if ($peminjaman->id_buku == $value->id) selected="selected" @endif>{{ $value->judul }}</option>
                            @endforeach
                          </select>

                          @error('buku')
                          <div class="alert alert-danger">
                              {{ $message }}
                          </div>
                          @enderror

                      </div>

                    <div class="form-group">
                        <label for="anggota">Nama Anggota</label>
                        <select name="anggota" id="anggota" class="form-control">
                          <option value="">--Pilih Nama Anggota--</option>
                          @foreach ($anggota as $item => $value)
                            <option value="{{ $value->id }}"
                              @if ($peminjaman->id_anggota == $value->id) selected="selected" @endif>{{ $value->nama }}</option>
                          @endforeach
                        </select>

                        @error('anggota')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror

                    </div>

                    <div class="form-group">
                      <label for="user">Nama Petugas</label>
                      <select name="user" id="user" class="form-control">
                        <option value="">--Pilih Nama Petugas--</option>
                        @foreach ($user as $item => $value)
                          <option value="{{ $value->id }}"
                            @if ($peminjaman->id_user == $value->id) selected="selected" @endif>{{ $value->name }}</option>
                        @endforeach
                      </select>

                      @error('user')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                      @enderror

                    <div class="form-group">
                        <label for="tanggal_pinjam" class="form-control-label">Tanggal Pinjam</label>
                        <input class="form-control" type="date" id="tanggal_pinjam" name="tanggal_pinjam" value="{{ $peminjaman->tanggal_pinjam }}" placeholder="Pilih Tahun">

                        @error('tanggal_pinjam')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror

                    </div>

                    <div class="form-group">
                        <label for="tanggal_kembali" class="form-control-label">Tanggal Kembali</label>
                        <input class="form-control" type="date" id="tanggal_kembali" name="tanggal_kembali" value="{{ $peminjaman->tanggal_kembali }}" placeholder="Pilih Tahun">

                        @error('tanggal_kembali')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror

                    </div>
                  </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update Data') }}
                            </button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
</div>
@endsection
@push('script')
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

  <script type="text/javascript">
    tinymce.init({
      selector: '#mytextarea'
    });
    </script>
@endpush
