@extends('layouts.master')
@push('style')
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css')}}">
@endpush

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Peminjaman</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Peminjaman</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
        <!-- Card stats -->
        <div class="container-fluid mt--6">

        <div class="row">
          {{-- <div class="col-xl-12 m-4"> --}}
            <div class="col">
                <a href="{{ route('peminjaman.create') }}" class="btn btn-warning mb-2 ">+ Tambah Peminjam</a>
              <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                  <h3 class="mb-0">Data Peminjaman</h3>
                </div>
                <!-- Light table -->
                <div class="table-responsive">
                  <table id="example1" class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" class="sort" data-sort="no">No</th>
                        <th scope="col" class="sort" data-sort="judul">Judul Buku</th>
                        <th scope="col" class="sort" data-sort="nama">Nama Anggota</th>
                        <th scope="col" class="sort" data-sort="name">Nama Petugas</th>
                        <th scope="col" class="sort" data-sort="tanggal_pinjam">Tanggal Pinjam</th>
                        <th scope="col" class="sort" data-sort="tanggal_kembali">Tanggal Kembali</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($peminjaman as $key => $value)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ substr($value->buku->judul,0,20) }} ...</td>
                            <td>{{ $value->anggota->nama }}</td>
                            <td>{{ $value->user->name }}</td>
                            <td>{{ $value->tanggal_pinjam }}</td>
                            <td>{{ $value->tanggal_kembali }}</td>
                            <td>
                                <form action="{{ route('peminjaman.destroy',$value->id) }}" method="POST">
                                    <a href="{{ route('peminjaman.show',$value->id) }}" class="btn btn-primary btn-sm">Show</a>
                                    <a href="{{ route('peminjaman.edit',$value->id) }}" class="btn btn-success btn-sm">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr scope="col">
                            <td>No data</td>
                        </tr>
                        @endforelse

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
      </div>
        </div>
</div>
@endsection
@push('script')
  <script src="{{asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
  <script src="{{asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js')}}"></script>
  <script>
    $(function () {
        $("#example1").DataTable();
    });

    @if (session('success'))
    swal.fire({
      title: 'Berhasil!',
      text: '{{ session('success') }}',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-success',
      icon: 'success'
    });
    @endif

  </script>
@endpush
